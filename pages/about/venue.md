---
name: The Conference Venue
---
# The Conference Venue

## Campus

FIXME

## Address

FIXME

## Wikipedia

* [University of Haifa](https://en.wikipedia.org/wiki/University_of_Haifa) (English)
* [אוניברסיטת חיפה](https://he.wikipedia.org/wiki/%D7%90%D7%95%D7%A0%D7%99%D7%91%D7%A8%D7%A1%D7%99%D7%98%D7%AA_%D7%97%D7%99%D7%A4%D7%94) (Hebrew)


## Maps

[OpenStreetMap](https://www.openstreetmap.org/way/55001924#map=16/32.7612/35.0202)

FIXME

## Rooms

FIXME

## Getting to the Venue

Flights land at Ben Gurion airport (TLV, 20 KM from Tel Aviv). From there the best option would be taking the Train to Haifa. It's about 1:20 to Haifa (Tel Aviv to haifa is 1 hour).

From Hafia's Hof Hacarmel train station you go (50 meters) to the central bus station (same name) to take bus 146 to the university. Bus stops: - Eshkol Tower stop for venue / front-desk - Multi-Purpose Building stop for the dormitories.


